### 1. Предназначение

[**Здесь**](https://gitlab.com/Kristinita/KiraCommunication/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) можете задавать вопросы автору [**Поиска Кристиниты**](http://cyclowiki.org/wiki/%D0%A3%D1%87%D0%B0%D1%81%D1%82%D0%BD%D0%B8%D0%BA%3A%D0%A1%D0%B0%D1%88%D0%B0_%D0%A7%D0%B5%D1%80%D0%BD%D1%8B%D1%85%2F%D0%9F%D0%BE%D0%B8%D1%81%D0%BA_%D0%9A%D1%80%D0%B8%D1%81%D1%82%D0%B8%D0%BD%D0%B8%D1%82%D1%8B) и [**Комнат Эрика**](https://kristinita.netlify.com/erics-rooms/%D0%BA%D0%BE%D0%BC%D0%BD%D0%B0%D1%82%D1%8B-%D1%8D%D1%80%D0%B8%D0%BA%D0%B0); сообщать ему о чём-либо.

### 2. Написание сообщений

Здесь применяется разметка [**Markdown**](https://paulradzkov.com/2014/markdown_cheatsheet/).

Отлично, если будете использовать возможности [**GitLab Flavored Markdown**](https://gitlab.com/help/user/markdown).

### 3. Правила

Пожалуйста, никаких агрессии, нецензурных выражений, оскорблений.

Приветствуются вдумчивые, подробные сообщения.